
# Running the service

## Option 1: Running directly on host OS
The following prerequisites need to be installed:

* Git >= 2.15
* Java JVM >= 8
* sbt >= 1.2

```
git clone git@bitbucket.org:marko_asplund/openinghours-ex.git
cd openinghours-ex
sbt run
```

## Option 2: Running in Docker container

Prerequisites:

* Git >= 2.15
* Docker (engine) >= 18.09.1 

```
git clone git@bitbucket.org:marko_asplund/openinghours-ex.git
cd openinghours-ex
docker-compose up
```

## Accessing the service

```
foobar:openinghours-ex $ curl -D - -X POST -H 'content-type: application/json' -d @src/test/resources/test1.json  localhost:8080/hours
HTTP/1.1 200 OK
Content-Type: text/plain; charset=UTF-8
Date: Thu, 01 Aug 2019 14:40:36 GMT
Content-Length: 142

Monday: Closed
Tuesday: 10 AM - 6 PM
Wednesday: Closed
Thursday: 10 AM - 6 PM
Friday: 10 AM - 1 AM
Saturday: 10 AM - 1 AM
Sunday: 12 PM - 9 PM
```


# Notes on the opening hours REST API data model design
Optimal data model design depends on the use cases, priorities of the use cases and which types of uses to optimise for.
Technical requirements can also play a significant role e.g. when network bandwidth, processing (esp. parsing on client end),
memory or storage resources are limited the data model might need to be optimised to accommodate for these requirements.
Client-side languages and parsing libraries / frameworks capabilities may also need to be considered.

Here're design comments specific to the opening hours data model:
* grouping both open and close events by day-of-week is somewhat confusing
  since day-of-week can be "incorrect" (from the assignment use case perspective) on close event objects
* data model is not optimal for generating a human readable description
  as described in the assignment since we need to infer the right day for close events
* the data model could also be optimised in terms of bytes used
* expressing that a restaurant is closed on a particular day of week using an empty array
  is technically redundant, but may be helpful to state the intent explicitly and also from a troubleshooting perspective
* the data model doesn't assumes a weekly recurring schedule and doesn't
  allow expressing exceptions (e.g. on specific dates off-season, holidays)
  or other than weekly recurrence schedule
* the client is assumed to know in which timezone to interpret the times in  


## Some alternative designs 

If we can assume that
a) generating the human readable text formatting for opening hours is the only
or primary use case to support and 
b) for each open event a corresponding close event exists and
c) the day of week is an attribute of the open, not the close event, these 3 pieces of information could
be bundled together e.g. as follows: 

```
[
  {
    "openDay": "friday",
    "open": 64800,
    "close": 3600
  },
  {
    "openDay": "saturday",
    "open": 32400,
    "close": 39600
  }
]
```

Grouping day, open and close times together in one opening hours entry
would better convey the semantics that these two times come in pairs and
that day is linked to opening time.

To make the the linking between opening time and day even more explicit
an alternative design could look as follows:
```
[
  {
    "open": {
      "day": "friday",
      "time": 64800
    },
    "close": 3600
  },
  {
    "open": {
      "day": "saturday",
      "time": 32400
    },
    "close": 39600
  }
]
```

If data representation size needs to be optimised
essentially the same logical model could also be represented 
using arrays to optimise data transfer / storage:
```
[
  ["friday", 64800, 3600],
  ["saturday", 32400, 39600]
]
```

These kinds of heterogeneous arrays could be challenging to parse in some
languages or parsing frameworks and to further optimise we could represent day-of-week as an integer
```
[
  [5, 64800, 3600],
  [6, 32400, 39600],
  [6, 57600, 82800]
]
```

This design would make the arrays homogeneous making parsing simple.
While the above design is quite optimised, this would essentially 
represent a tabular data model in JSON and some other data formats 
might be more suitable for tabular data.
 