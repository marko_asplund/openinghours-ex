package com.practicingtechie.ex.openinghours

import org.specs2.mutable._

class OpeningHoursFormatterSpec extends Specification {
  import cats.effect.{IO, Resource, Sync}
  import scala.language.higherKinds

  val TestCaseFileBaseDir = "src/test/resources"
  val InputFileSuffix = ".json"
  val ResultFileSuffix = ".txt"
  val Utf8 = "UTF-8"

  def getTestCaseFileBaseNames(baseDir: String) =
    new java.io.File(baseDir).listFiles.toList
      .filter(f => f.getName.endsWith(InputFileSuffix))
      .map(f => new java.io.File(f.getPath.replace(InputFileSuffix, "")))

  def getTestCase[F[_]](baseFileName: String)(implicit F: Sync[F]) = {
    import cats.implicits._
    import cats.data.EitherT
    import io.circe.parser._
    import model._
    import JsonCodec._
    import java.io._

    def source(fn: String) = Resource.fromAutoCloseable(F.delay {
      scala.io.Source.fromFile(new File(fn), Utf8)
    })
    def parseHours(fn: String) =
      source(fn).use(s => decode[OpeningHours](s.mkString).leftMap(_.getMessage).pure[F])
    def readFully(fn: String) = source(fn).use(_.mkString.pure[F])

    (for {
      input <- EitherT(parseHours(s"${baseFileName}${InputFileSuffix}"))
      expected <- EitherT.right[String](readFully(s"${baseFileName}${ResultFileSuffix}"))
    } yield input -> expected).value
  }

  getTestCaseFileBaseNames(TestCaseFileBaseDir) foreach { testFile =>
    s"pass test case '${testFile.getName}'" >> {
      getTestCase[IO](testFile.getAbsolutePath).unsafeRunSync() match {
        case Left(errMsg) => ko(s"ERROR: $errMsg")
        case Right((input, expected)) => OpeningHoursFormatter.formatHours(input) === expected
      }
    }
  }

}
