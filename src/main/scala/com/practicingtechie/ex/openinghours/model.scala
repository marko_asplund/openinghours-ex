package com.practicingtechie.ex.openinghours

import model.DayOfWeek.DayOfWeek
import model.EntryType.EntryType
import java.time.LocalTime


package model {
  object EntryType extends Enumeration {
    type EntryType = Value
    val Open = Value("open")
    val Close = Value("close")
  }

  object DayOfWeek extends Enumeration {
    type DayOfWeek = Value
    val Monday = Value("monday")
    val Tuesday = Value("tuesday")
    val Wednesday = Value("wednesday")
    val Thursday = Value("thursday")
    val Friday = Value("friday")
    val Saturday = Value("saturday")
    val Sunday = Value("sunday")
  }

  case class OpeningHoursEntry(`type`: EntryType, value: LocalTime)

  case class OpeningHours(spec: Map[DayOfWeek, List[OpeningHoursEntry]])
}


object JsonCodec {
  import cats.effect.Sync
  import cats.syntax.either._
  import io.circe.Decoder
  import io.circe.KeyDecoder
  import io.circe.generic.semiauto._
  import model._
  import org.http4s.circe.jsonOf
  import scala.language.higherKinds

  implicit val dayOfWeekDecoder = Decoder.enumDecoder(DayOfWeek)
  implicit val entryTypeDecoder = Decoder.enumDecoder(EntryType)
  implicit val dayOfWeekKeyDecoder = new KeyDecoder[DayOfWeek] {
    override def apply(key: String): Option[DayOfWeek] = Some(DayOfWeek.withName(key))
  }
  implicit val decodeInstant = Decoder.decodeLong.emap { s =>
    Either.catchNonFatal(LocalTime.ofSecondOfDay(s)).leftMap(_.toString)
  }
  implicit val openingHoursEntryDecoder: Decoder[OpeningHoursEntry] = deriveDecoder
  implicit val openingHoursDecoder =
    Decoder.decodeMap[DayOfWeek, List[OpeningHoursEntry]].emap(d => Right(OpeningHours(d)))
  implicit def jsonDecoder[F[_] : Sync, A <: Product : Decoder] = jsonOf[F, A]
}