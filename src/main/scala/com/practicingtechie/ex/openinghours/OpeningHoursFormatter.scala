package com.practicingtechie.ex.openinghours

object OpeningHoursFormatter {
  import model._
  import DayOfWeek.DayOfWeek
  import java.time.LocalTime

  val df = java.time.format.DateTimeFormatter.ofPattern("h a")

  def formatHours(hours: OpeningHours) = {

    def formatHoursAsString(openingHours: Map[DayOfWeek, Seq[(DayOfWeek, LocalTime, LocalTime)]]) = {
      def upper1(s: String) = s.substring(0, 1).toUpperCase + s.substring(1)

      val closedDays = (DayOfWeek.values.toSet -- openingHours.keys.toSet).map(d => d -> List.empty)

      (openingHours ++ closedDays).map {
        case (day, Nil) => day -> s"${upper1(day.toString)}: Closed"
        case (day, hours) =>
          val hrs = hours.map {
            case (_, open, close) => s"${df.format(open)} - ${df.format(close)}"
          }.mkString(", ")
          day -> s"${upper1(day.toString)}: $hrs"
      }.toList.sorted.map{case ((_, hours)) => hours}.mkString("\n")
    }

    def generateOpeningHoursEntries(openingHours: OpeningHours) =
      openingHours.spec.toList.sortBy { case (day, _) => day }.flatMap {
        case (day, hours) => hours.sortBy(_.value).map(i => day -> i.value)
      }.grouped(2).collect {
        case (day, open) :: (_, close) :: Nil => (day, open, close)
      }.toSeq.groupBy { case (day, _, _) => day }

    formatHoursAsString(generateOpeningHoursEntries(hours))
  }

}
