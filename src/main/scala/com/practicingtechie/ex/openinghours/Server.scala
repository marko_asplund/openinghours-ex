package com.practicingtechie.ex.openinghours

import cats.effect._
import org.http4s.syntax.kleisli._
import scala.language.higherKinds

class OpeningHoursServer[F[_] : ConcurrentEffect : Timer](port: Int, bindAddress: String) {
  import org.http4s.server.middleware.CORS
  import org.http4s.server.blaze.BlazeServerBuilder
  import org.http4s.server.Router

  def builder = BlazeServerBuilder[F]
    .bindHttp(port, bindAddress)
    .withHttpApp(CORS(Router(
      "/hours" -> (new OpeningHoursEndpoint).routes
    ).orNotFound))
}

object Server extends IOApp {
  import cats.implicits._

  val DefaultBindAddress = "0.0.0.0"
  val DefaultPort = 8080

  def run(args: List[String]): IO[ExitCode] =
    new OpeningHoursServer[IO](DefaultPort, DefaultBindAddress).builder
      .serve.compile.drain.as(ExitCode.Success)
}
