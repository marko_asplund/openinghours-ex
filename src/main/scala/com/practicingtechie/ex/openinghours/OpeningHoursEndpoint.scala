package com.practicingtechie.ex.openinghours

import cats.effect.Sync
import org.http4s.dsl.Http4sDsl
import scala.language.higherKinds


class OpeningHoursEndpoint[F[_]: Sync] extends Http4sDsl[F] {
  import com.typesafe.scalalogging.Logger
  import org.http4s.HttpRoutes
  import model._
  import JsonCodec._
  import cats.implicits._

  val logger = Logger(this.getClass)

  val routes: HttpRoutes[F] = HttpRoutes.of[F] {
    case req@POST -> Root =>
      req.attemptAs[OpeningHours].value flatMap {
        case Right(hours) => Ok(OpeningHoursFormatter.formatHours(hours))
        case Left(th) => InternalServerError(th.getMessage)
      }
  }

}
