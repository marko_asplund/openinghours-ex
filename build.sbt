organization  := "com.practicingtechie"

name := "openinghours-ex"

version := "0.0.1"

scalaVersion := "2.12.8"

scalacOptions := Seq("-feature",
  "-encoding", "utf8",
  "-deprecation",
  "-unchecked",
  "-Xlint",
  "-Yrangepos",
)

val scalaLoggingV = "3.9.0"
val logBackV = "1.2.3"
val http4sV = "0.20.7"
val circeV = "0.11.1"
val specs2V = "4.6.0"

libraryDependencies ++= Seq(
  "com.typesafe.scala-logging" %% "scala-logging" % scalaLoggingV,
  "ch.qos.logback"       % "logback-classic"      % logBackV % "runtime",
  "io.circe"            %% "circe-generic"        % circeV,
  "io.circe"            %% "circe-literal"        % circeV,
  "io.circe"            %% "circe-generic-extras" % circeV,
  "io.circe"            %% "circe-parser"         % circeV % "test",
  "org.http4s"          %% "http4s-dsl"           % http4sV,
  "org.http4s"          %% "http4s-blaze-server"  % http4sV,
  "org.http4s"          %% "http4s-circe"         % http4sV,
  "org.specs2"          %% "specs2-core"          % specs2V % "test"
)

mainClass in (Compile, run) := Some("com.practicingtechie.ex.openinghours.Server")

enablePlugins(JavaAppPackaging)
