
# ===============
FROM alpine:3.10 AS base-java-img
RUN apk update && apk upgrade
RUN apk add bash openjdk11


# ===============
FROM base-java-img AS openinghours-build

ADD https://piccolo.link/sbt-1.2.8.tgz /opt/sbt.tgz
RUN cd /opt && tar -xzf sbt.tgz && rm sbt.tgz && ln -s /opt/sbt/bin/sbt /usr/local/bin/

COPY ./ /opt/app
RUN cd /opt/app && sbt universal:packageZipTarball


# ===============
FROM base-java-img AS openinghours-runtime
WORKDIR /opt/app
COPY --from=1 /opt/app/target/universal/openinghours-ex-*.tgz openinghours-ex.tgz
RUN mkdir openinghours-ex
RUN tar --strip-components 1 -zxf openinghours-ex.tgz -C openinghours-ex
CMD ./openinghours-ex/bin/openinghours-ex
